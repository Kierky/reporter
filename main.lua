-- reporter: make sure the whole guild knows of your success and shame
-- Original idea by Harpo
-- modified heavily & redesigned by Estrian
-- v1.5a Fixed a bug reporting yourself as killer and uses proper /me tags
-- v1.6 Made plugin fully automatic with group toggles
-- v1.7 included multi-kill buffer and version. Major cleanup of code and a few unnecessary function removals. 
-- v1.7.2 bug fixes

declare('reporter', {})

reporter.toggle={}
reporter.Init={}
reporter.someDeath={}
reporter.GroupEvents={}
reporter.ArgToggle={}
reporter.broadcast = {}
reporter.cache = {}

-- Vars
function reporter.Init()
	if IsGroupMember(GetCharacterID()) == true then
		reporter.pluginState = false
		print("Reporter Initialised while in group, turning reporter off..")
	else
		reporter.pluginState = true
		print("Reporter Initialised")
	end
end

reporter.version = "1.7.2"
reporter.timer = Timer()

-- colours
reporter.groupcolour = "\127FFFFFF"
reporter.truecolour = "\12733CC33"
reporter.falsecolour = "\127CC0000"

function reporter.ArgToggle()
	if reporter.pluginState == true then
		reporter.pluginState = false
		print(reporter.falsecolour .. "reporter toggled, OFF\127o")
	else 
		reporter.pluginState = true
		print(reporter.truecolour .. "reporter toggled, ON\127o")
	end
end

function reporter.toggle(data,args)
	if not args then
		print(reporter.groupcolour .. "Commands are /reporter [toggle/version]\127o")
	elseif args[1] == "toggle" then
		reporter.ArgToggle()
	elseif args[1] == "version" then
		print(reporter.groupcolour .. "Reporter version ".. reporter.version .. "\127o")
	else
		print(reporter.groupcolour .. "Not a valid argument\127o")
	end
end

function reporter.GroupEvents:OnEvent(eventname, args)
	if eventname=='GROUP_CREATED' or eventname =='GROUP_SELF_JOINED' then
		reporter.pluginState = false
		print('\127ffffffreporter Autotoggled to OFF (group joined)\127o')
	elseif eventname=='GROUP_SELF_LEFT' then
		reporter.pluginState = true
		print(reporter.truecolour .. "reporter Auto-toggled to ON (group left)\127o")
	end
end

function reporter.someDeath:OnEvent(event, deadguyID, killerID)
	if reporter.pluginState == true then
		if (not GetPlayerName(deadguyID):match("^%*")) then
			if (not GetPlayerName(killerID):match("^%*")) then
				local selfID = GetCharacterID()
				local message
				if (deadguyID == killerID) then
					message = "/me used some form of self-destruction!"
					reporter.appendstring(message)
				elseif (deadguyID == selfID) and (killerID ~= selfID) then
					local killer = GetPlayerName(killerID)
					message = "/me was destroyed by ".. killer
					reporter.appendstring(message)
				else
					local deadguy = GetPlayerName(deadguyID) or "none"
					local killer = GetPlayerName(killerID) or "none"
					message = "/me destroyed " .. deadguy
					reporter.appendstring(message)
				end
			end
		end
	end
end

function reporter.broadcast()
	if #reporter.cache > 0 then
		reporter.timer:SetTimeout(1100, function()
			local i = table.remove(reporter.cache)
			SendChat(i, "GUILD")
			reporter.broadcast()
		end)
	end 
end

function reporter.appendstring(message)
	table.insert(reporter.cache, message)
	reporter.broadcast()
end

RegisterEvent(reporter.someDeath, "PLAYER_DIED")
RegisterEvent(reporter.GroupEvents, "GROUP_CREATED")
RegisterEvent(reporter.GroupEvents, "GROUP_SELF_LEFT")
RegisterEvent(reporter.GroupEvents, "GROUP_SELF_JOINED")

RegisterUserCommand('reporter', reporter.toggle)
reporter.Init()